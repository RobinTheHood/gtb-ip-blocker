# GTB IP Blocker


## Name
IP automatisch auf Blacklist setzen <br> 
(based on modul from GTB - member of modifiedShop development team)

## Description
Es können damit im Admin URL Pfade eingetragen und die Blockingzeit definiert werden welche bei Aufruf auf die Blacklist gesetzt werden.
Ich habe es erweitert um eine Anzeige wie oft und wann zuletzt der Pfad erkannt wurde.

Die Pfade werden nur verglichen, wenn der Error Handler eingebunden wird, also wenn eine URL nicht gefunden wird und ein Error Request (wie in der htaccess definiert) vorhanden ist.

## Hint
This project is also used to test usage of Git. Therefore, the gitlog could be looking strange.

## Installation
1. Inhalt des Ordners "NEW_FILES" in das Shopverzeichnis hochladen.

2. Auto Include einfügen für Shopversionen bis einschliesslich 2.0.7.2

   /media/content/sitemap.php

fügen Sie vor dem hier:
```
$module_smarty = new smarty;
```
das hier ein:
```
foreach(auto_include(DIR_FS_CATALOG.'includes/extra/modules/error_handler/','php') as $file) require_once ($file);
```
3. Im Adminbereich unter "Module" -> "System Module" finden Sie das Modul "IP Blocker" zum Installieren.

4. Unter "Hilfsprogramme" -> "IP Blocker" können sie die URL Pfade konfigurieren

## Support
[modified-shop Forum](https://www.modified-shop.org/forum/index.php?topic=43152.0)

## Roadmap
- [ ] option to enable/disable entries
- [ ] auto adding entries to database
- [ ] easier import from keyword lists

## Contributing
Changes can be requested in [modified-shop Forum](https://www.modified-shop.org/forum/index.php?topic=43152.0) or via GitLab Project

## Authors and acknowledgment
Based on GTB [IP-Blocker](https://www.modified-shop.org/forum/index.php?topic=43152.0)
modified eCommerce Shopsoftware
http://www.modified-shop.org

Copyright (c) 2009 - 2013 [www.modified-shop.org]

## License
GNU General Public License
Version 2, June 1991
Copyright (C) 1989, 1991 Free Software Foundation, Inc.
675 Mass Ave, Cambridge, MA 02139, USA

