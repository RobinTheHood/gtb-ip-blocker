<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  define('MODULE_IP_BLOCKER_TEXT_TITLE', 'IP Blocker');
  define('MODULE_IP_BLOCKER_TEXT_DESCRIPTION', 'The IP blocker is used to block IP addresses that call up a defined URL path.');
  
  define('MODULE_IP_BLOCKER_STATUS_TITLE', 'Activate Module?');
  define('MODULE_IP_BLOCKER_STATUS_DESC', 'Activate IP Blocker');
  define('MODULE_IP_BLOCKER_DEFAULT_TIME', 'Default time');
  define('MODULE_IP_BLOCKER_DEFAULT_TIME_DESC', 'Default time for blocking in seconds. \<br\> 3600 = 1h; 21600 = 6h; 43200 = 12h; 86400 = 24h');

