<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  define('HEADING_TITLE', 'IP Blocker');

  define('TABLE_HEADING_BLOCKING_PATH', 'Path');
  define('TABLE_HEADING_BLOCKING_TIME', 'Time');
  define('TABLE_HEADING_BLOCKING_COUNT', 'Hits');
  define('TABLE_HEADING_ACTION', 'Action');

  define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
  define('TEXT_INFO_IP_BLOCKER_BLOCKING_PATH', 'Path:');
  define('TEXT_INFO_IP_BLOCKER_BLOCKING_TIME', 'Time:');

  define('TEXT_INFO_INSERT_INTRO', 'Please enter the all related data');
  define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this path?');
  define('TEXT_INFO_HEADING_NEW_IP_BLOCKER', 'New path');
  define('TEXT_INFO_HEADING_EDIT_IP_BLOCKER', 'Edit path');
  define('TEXT_INFO_HEADING_DELETE_IP_BLOCKER', 'Delete path');

  define('TEXT_INFO_DATE_ADDED', 'date added:');
  define('TEXT_INFO_LAST_VISTIED', 'last blocked:');

  define('TEXT_ONE_HOUR', '1h');
  define('TEXT_SIX_HOUR', '6h');
  define('TEXT_TWELVE_HOUR', '12h');
  define('TEXT_TWENTYFOUR_HOUR', '24h');

  define('TEXT_DISPLAY_NUMBER_OF_IP_BLOCKER', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> Paths)');
