<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  define('MODULE_IP_BLOCKER_TEXT_TITLE', 'IP Blocker');
  define('MODULE_IP_BLOCKER_TEXT_DESCRIPTION', 'Mit dem IP Blocker werden die IP Adressen gesperrt, welche einen definierten URL Pfad aufrufen.');
  
  define('MODULE_IP_BLOCKER_STATUS_TITLE', 'Modul aktivieren?');
  define('MODULE_IP_BLOCKER_STATUS_DESC', 'IP Blocker aktivieren');
  define('MODULE_IP_BLOCKER_DEFAULT_TIME_TITLE', 'Standardzeit');
  define('MODULE_IP_BLOCKER_DEFAULT_TIME_DESC', 'Voreingestellte Zeit zum blockieren in Sekunden. <br> 3600 = 1h<br> 21600 = 6h<br> 43200 = 12h<br> 86400 = 24h');
