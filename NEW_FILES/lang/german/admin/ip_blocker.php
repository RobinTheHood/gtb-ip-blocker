<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  define('HEADING_TITLE', 'IP Blocker');

  define('TABLE_HEADING_BLOCKING_PATH', 'Pfad');
  define('TABLE_HEADING_BLOCKING_TIME', 'Zeit');
  define('TABLE_HEADING_BLOCKING_COUNT', 'Hits');
  define('TABLE_HEADING_ACTION', 'Aktion');

  define('TEXT_INFO_EDIT_INTRO', 'Bitte f&uuml;hren Sie notwendige &Auml;nderungen durch');
  define('TEXT_INFO_IP_BLOCKER_BLOCKING_PATH', 'Pfad:');
  define('TEXT_INFO_IP_BLOCKER_BLOCKING_TIME', 'Zeit:');

  define('TEXT_INFO_INSERT_INTRO', 'Bitte geben Sie alle relevanten Daten ein');
  define('TEXT_INFO_DELETE_INTRO', 'Sind Sie sicher, dass Sie diesen Pfad l&ouml;schen m&ouml;chten?');
  define('TEXT_INFO_HEADING_NEW_IP_BLOCKER', 'Neuer Pfad');
  define('TEXT_INFO_HEADING_EDIT_IP_BLOCKER', 'Pfad bearbeiten');
  define('TEXT_INFO_HEADING_DELETE_IP_BLOCKER', 'Pfad l&ouml;schen');

  define('TEXT_INFO_DATE_ADDED', 'hinzugef&uuml;gt am:');
  define('TEXT_INFO_LAST_VISTIED', 'zuletzt geblockt am:');

  define('TEXT_ONE_HOUR', '1h');
  define('TEXT_SIX_HOUR', '6h');
  define('TEXT_TWELVE_HOUR', '12h');
  define('TEXT_TWENTYFOUR_HOUR', '24h');

  define('TEXT_DISPLAY_NUMBER_OF_IP_BLOCKER', 'Angezeigt werden <b>%d</b> bis <b>%d</b> (von insgesamt <b>%d</b> Pfade)');
