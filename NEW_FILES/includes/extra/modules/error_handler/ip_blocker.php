<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  if (defined('MODULE_IP_BLOCKER_STATUS')
      && MODULE_IP_BLOCKER_STATUS == 'true'
      && isset($_REQUEST['error'])
      && $_REQUEST['error'] == 404
      )
  {
    require_once(DIR_FS_CATALOG.'includes/xss_secure.php');
    
    $blocker_query = xtDBquery("SELECT *
                                  FROM `ip_blocking`");
    if (xtc_db_num_rows($blocker_query, true) > 0) {
      while ($blocker = xtc_db_fetch_array($blocker_query, true)) {
        if (strpos($_SERVER['REQUEST_URI'], $blocker['blocking_path']) !== false) {          
          $blacklist_time = time() + $blocker['blocking_time'] - XSS_BLACKLIST_TIME;
          $_SERVER['HTTP_USER_AGENT'] = gethostbyaddr($_SESSION['tracking']['ip']);
          
          if (xtc_check_agent() == 0
              && $_SESSION['tracking']['ip'] != '' && $blacklist_time > 0
              )
          {            
            xtc_db_query("UPDATE `ip_blocking`
                             SET blocking_count = blocking_count + 1,
                                 last_visited = now()
                           WHERE blocking_id = '".(int)$blocker['blocking_id']."'");
            
            $contents_array = xss_read_blacklist();
            $contents_array[$_SESSION['tracking']['ip']] = $blacklist_time;
            xss_write_blacklist($contents_array);
            xtc_redirect(xtc_href_link(FILENAME_DEFAULT, '', $request_type));
          }
        }
      }
    }                         
  }
