<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );

  class ip_blocker {
    var $code, $title, $description, $enabled;

    function __construct() {
       $this->code = 'ip_blocker';
       $this->title = MODULE_IP_BLOCKER_TEXT_TITLE;
       $this->description = MODULE_IP_BLOCKER_TEXT_DESCRIPTION;
       $this->sort_order = defined('MODULE_IP_BLOCKER_SORT_ORDER') ? MODULE_IP_BLOCKER_SORT_ORDER : '';
       $this->enabled = ((defined('MODULE_IP_BLOCKER_STATUS') && MODULE_IP_BLOCKER_STATUS == 'true') ? true : false);
    }

    function process($file) {
    }

    function display() {
      return array('text' => '<br /><div align="center">' . xtc_button(BUTTON_SAVE) .
                             xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_MODULE_EXPORT, 'set=' . $_GET['set'] . '&module=ip_blocking')) . "</div>");
    }

    function check() {
      if (!isset($this->_check)) {
        if (defined('MODULE_IP_BLOCKER_STATUS')) {
          $this->_check = true;
        } else {
          $check_query = xtc_db_query("SELECT configuration_value 
                                         FROM " . TABLE_CONFIGURATION . " 
                                        WHERE configuration_key = 'MODULE_IP_BLOCKER_STATUS'");
          $this->_check = xtc_db_num_rows($check_query);
        }
      }
      return $this->_check;
    }
    
    function install() {
      xtc_db_query("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) VALUES 
            ('MODULE_IP_BLOCKER_STATUS', 'true',  '6', '1', 'xtc_cfg_select_option(array(\'true\', \'false\'), ', now()) ,
            ('MODULE_IP_BLOCKER_DEFAULT_TIME', '3600',  '6', '1', 'xtc_cfg_select_option(array(\'3600\', \'21600\', \'43200\', \'86400\'), ', now())      
      ");

//array(\'id\' => 3600, \'text\' => \'1h\'),
//array(\'id\' => 21600, \'text\' => \'6h\'),
//array(\'id\' => 43200, \'text\' => \'12h\'),
//array(\'id\' => 86400, \'text\' => \'24h\'),
//            ('MODULE_IP_BLOCKER_DEFAULT_TIME', '1h',  '6', '1', 'xtc_cfg_select_option(array(\'1h\', \'6h\', \'12h\', \'24h\'), ', now())

      xtc_db_query("CREATE TABLE IF NOT EXISTS ip_blocking (
                        blocking_id    INT AUTO_INCREMENT                   PRIMARY KEY,
                        blocking_path  VARCHAR(32)                          NOT NULL,
                        blocking_time  INT      DEFAULT 3600                NOT NULL,
                        blocking_count INT      DEFAULT 0                   NOT NULL,
                        date_added     DATETIME DEFAULT CURRENT_TIMESTAMP() NOT NULL,
                        last_visited   DATETIME DEFAULT (0 - 0 - 0)         NOT NULL,
                        CONSTRAINT ip_blocking_pk
                        UNIQUE (blocking_path)
                    )");
      
      $db_table_rows = array();
      $query_result = xtc_db_query("SHOW COLUMNS FROM `" . TABLE_ADMIN_ACCESS . "`");
      while ($row = xtc_db_fetch_array($query_result)) {
        $db_table_rows[] = $row['Field'];
      }
      
      if (!in_array('ip_blocker', $db_table_rows)) {
        xtc_db_query("ALTER TABLE `" . TABLE_ADMIN_ACCESS . "` ADD `ip_blocker` INT(1) NOT NULL DEFAULT 0");
        xtc_db_query("UPDATE `" . TABLE_ADMIN_ACCESS . "` SET `ip_blocker` = 1 WHERE `customers_id` = 1");
        xtc_db_query("UPDATE `" . TABLE_ADMIN_ACCESS . "` SET `ip_blocker` = 1 WHERE `customers_id` = ".$_SESSION['customer_id']);
        xtc_db_query("UPDATE `" . TABLE_ADMIN_ACCESS . "` SET `ip_blocker` = 9 WHERE `customers_id` = 'groups'");
      }
    }

    function remove() {
      xtc_db_query("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key in ('" . implode("', '", $this->keys()) . "')");
      
      $check_query = xtc_db_query("SELECT *
                                     FROM `ip_blocking`");
      if (xtc_db_num_rows($check_query) < 1) {
        xtc_db_query("DROP TABLE `ip_blocking`");
      }
    }

    function keys() {
      $key = array(
        'MODULE_IP_BLOCKER_STATUS',
        'MODULE_IP_BLOCKER_DEFAULT_TIME',
      );

      return $key;
    }
  }
