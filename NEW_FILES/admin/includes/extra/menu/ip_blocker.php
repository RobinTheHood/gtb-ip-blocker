<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('MODULE_IP_BLOCKER_STATUS') || MODULE_IP_BLOCKER_STATUS != 'true') {
  return;
}

$add_contents[BOX_HEADING_TOOLS][] = [
  'admin_access_name' => 'ip_blocker',
  'filename' => 'ip_blocker.php',
  'boxname' => 'IP Blocker',
  'parameters' => '',
  'ssl' => ''
];
