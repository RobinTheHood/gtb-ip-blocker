<?php
/* -----------------------------------------------------------------------------------------
   $Id$

   modified eCommerce Shopsoftware
   http://www.modified-shop.org

   Copyright (c) 2009 - 2013 [www.modified-shop.org]
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

  require('includes/application_top.php');

  //display per page
  $cfg_max_display_results_key = 'MAX_DISPLAY_IP_BLOCKER_RESULTS';
  $page_max_display_results = xtc_cfg_save_max_display_results($cfg_max_display_results_key);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $page = (isset($_GET['page']) ? (int)$_GET['page'] : 1);
  
  $blocking_time_array = array();
  $blocking_time_array[] = array('id' => 3600, 'text' => TEXT_ONE_HOUR);
  $blocking_time_array[] = array('id' => 21600, 'text' => TEXT_SIX_HOUR);
  $blocking_time_array[] = array('id' => 43200, 'text' => TEXT_TWELVE_HOUR);
  $blocking_time_array[] = array('id' => 86400, 'text' => TEXT_TWENTYFOUR_HOUR);
  
  switch ($action) {
    case 'insert':
    case 'save':
      $bID = ((isset($_GET['bID'])) ? (int)$_GET['bID'] : 0);
      
      $sql_data_array = array(
        'blocking_path' => xtc_db_prepare_input($_POST['blocking_path']),
        'blocking_time' => xtc_db_prepare_input($_POST['blocking_time']),
      );
      
      if ($action == 'insert') {
        $sql_data_array['date_added'] = 'now()';
        xtc_db_perform('ip_blocking', $sql_data_array);
        $bID = xtc_db_insert_id();
      } elseif ($action == 'save') {
        xtc_db_perform('ip_blocking', $sql_data_array, 'update', "blocking_id = '".$bID."'");
      }      
      xtc_redirect(xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . 'bID=' . $bID));
      break;

    case 'deleteconfirm':
      $bID = (int)$_GET['bID'];
      xtc_db_query("DELETE FROM `ip_blocking` WHERE blocking_id = '".$bID."'");

      xtc_redirect(xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action'))));
      break;
  }
  
  require (DIR_WS_INCLUDES.'head.php');
?>
<script type="text/javascript" src="includes/general.js"></script>
</head>
<body>
  <!-- header //-->
  <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
  <!-- header_eof //-->
  <!-- body //-->
  <table class="tableBody">
    <tr>
      <?php //left_navigation
      if (USE_ADMIN_TOP_MENU == 'false') {
        echo '<td class="columnLeft2">'.PHP_EOL;
        echo '<!-- left_navigation //-->'.PHP_EOL;       
        require_once(DIR_WS_INCLUDES . 'column_left.php');
        echo '<!-- left_navigation eof //-->'.PHP_EOL; 
        echo '</td>'.PHP_EOL;      
      }
      ?>
      <!-- body_text //-->
      <td class="boxCenter">
        <div class="pageHeadingImage"><?php echo xtc_image(DIR_WS_ICONS.'heading/icon_configuration.png'); ?></div>
        <div class="pageHeading"><?php echo HEADING_TITLE; ?></div>       
        <div class="main pdg2 flt-l">Configuration</div>       
        <table class="tableCenter">      
          <tr>
            <td class="boxCenterLeft">
              <table class="tableBoxCenter collapse">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo 'ID'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_BLOCKING_PATH; ?></td>
                <td class="dataTableHeadingContent txta-r"><?php echo TABLE_HEADING_BLOCKING_TIME; ?></td>
                <td class="dataTableHeadingContent txta-r"><?php echo TABLE_HEADING_BLOCKING_COUNT; ?></td>
                <td class="dataTableHeadingContent txta-r"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
              <?php
                $blocking_query_raw = "SELECT * 
                                         FROM `ip_blocking`";
                $blocking_split = new splitPageResults($page, $page_max_display_results, $blocking_query_raw, $blocking_query_numrows, 'blocking_id');
                $blocking_query = xtc_db_query($blocking_query_raw);
                while ($blocking = xtc_db_fetch_array($blocking_query)) {
                  if ((!isset($_GET['bID']) || $_GET['bID'] == $blocking['blocking_id']) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
                    $oInfo = new objectInfo($blocking);
                  }

                  if (isset($oInfo) && is_object($oInfo) && $blocking['blocking_id'] == $oInfo->blocking_id) {
                    echo '<tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'pointer\'" onclick="document.location.href=\'' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . 'bID=' . $oInfo->blocking_id . '&action=edit') . '\'">' . "\n";
                  } else {
                    echo '<tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'pointer\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . 'bID=' . $blocking['blocking_id']) . '\'">' . "\n";
                  }
                ?>
                <td class="dataTableContent"><?php echo $blocking['blocking_id']; ?></td>
                <td class="dataTableContent"><?php echo $blocking['blocking_path']; ?></td>
                <td class="dataTableContent txta-r"><?php echo ($blocking['blocking_time'] / 3600); ?>h</td>
                <td class="dataTableContent txta-r"><?php echo $blocking['blocking_count']; ?></td>
                <td class="dataTableContent txta-r"><?php if (isset($oInfo) && is_object($oInfo) && $blocking['blocking_id'] == $oInfo->blocking_id) { echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ICON_ARROW_RIGHT); } else { echo '<a href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID')) . 'bID=' . $blocking['blocking_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_arrow_grey.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
              <?php
                }
              ?>
              </table>
              
              <div class="smallText pdg2 flt-l"><?php echo $blocking_split->display_count($blocking_query_numrows, $page_max_display_results, $page, TEXT_DISPLAY_NUMBER_OF_IP_BLOCKER); ?></div>
              <div class="smallText pdg2 flt-r"><?php echo $blocking_split->display_links($blocking_query_numrows, $page_max_display_results, MAX_DISPLAY_PAGE_LINKS, $page); ?></div>
              <?php echo draw_input_per_page($PHP_SELF,$cfg_max_display_results_key,$page_max_display_results); ?>

              <?php
              if (substr($action, 0, 3) != 'new') {
              ?>
              <div class="clear"></div>
              <div class="pdg2 flt-r smallText"><?php echo '<a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . '&action=new') . '">' . BUTTON_INSERT . '</a>'; ?></div>
              <?php
              }
              ?>
            </td>
          <?php
            $heading = array();
            $contents = array();
            switch ($action) {
              case 'new':
                $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_IP_BLOCKER . '</b>');

                $contents = array('form' => xtc_draw_form('status', basename($PHP_SELF), xtc_get_all_get_params(array('action')) . 'action=insert'));
                $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
                $contents[] = array('text' => '<br />' . TEXT_INFO_IP_BLOCKER_BLOCKING_PATH . '<br />' . xtc_draw_input_field('blocking_path', ''));
                $contents[] = array('text' => '<br />' . TEXT_INFO_IP_BLOCKER_BLOCKING_TIME . '<br />' . xtc_draw_pull_down_menu('blocking_time', $blocking_time_array, MODULE_IP_BLOCKER_DEFAULT_TIME));
                $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_INSERT . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action'))) . '">' . BUTTON_CANCEL . '</a>');
                break;

              case 'edit':
                $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_IP_BLOCKER . '</b>');

                $contents = array('form' => xtc_draw_form('status', basename($PHP_SELF), xtc_get_all_get_params(array('action', 'bID')) . '&bID=' . $oInfo->blocking_id  . '&action=save'));
                $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
                $contents[] = array('text' => '<br />' . TEXT_INFO_IP_BLOCKER_BLOCKING_PATH . '<br />' . xtc_draw_input_field('blocking_path', $oInfo->blocking_path));
                $contents[] = array('text' => '<br />' . TEXT_INFO_IP_BLOCKER_BLOCKING_TIME . '<br />' . xtc_draw_pull_down_menu('blocking_time', $blocking_time_array, $oInfo->blocking_time));
                $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_UPDATE . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'bID')) . 'bID=' . $oInfo->blocking_id) . '">' . BUTTON_CANCEL . '</a>');
                break;

              case 'delete':
                $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_IP_BLOCKER . '</b>');

                $contents = array('form' => xtc_draw_form('status', basename($PHP_SELF), xtc_get_all_get_params(array('action', 'bID')) . 'bID=' . $oInfo->blocking_id  . '&action=deleteconfirm'));
                $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
                $contents[] = array('text' => '<br /><b>' . $oInfo->blocking_path . '</b>');
                $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_DELETE . '"/> <a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('action', 'bID')) . 'bID=' . $oInfo->blocking_id) . '">' . BUTTON_CANCEL . '</a>');
                break;

              default:
                if (isset($oInfo) && is_object($oInfo)) {
                  $heading[] = array('text' => '<b>' . $oInfo->blocking_path . '</b>');

                  $contents[] = array('align' => 'center', 'text' => '<a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . 'bID=' . $oInfo->blocking_id . '&action=edit') . '">' . BUTTON_EDIT . '</a> <a class="button" onclick="this.blur();" href="' . xtc_href_link(basename($PHP_SELF), xtc_get_all_get_params(array('bID', 'action')) . 'bID=' . $oInfo->blocking_id . '&action=delete') . '">' . BUTTON_DELETE . '</a>');
                  $contents[] = array('text' => '<br />' . TEXT_INFO_DATE_ADDED . ' ' . xtc_date_short($oInfo->date_added));
                  if (strtotime($oInfo->last_visited) > 0) {
                    $contents[] = array('text' => TEXT_INFO_LAST_VISTIED . ' ' . xtc_date_short($oInfo->last_visited));
                  }
                }
                break;
            }

            if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
              echo '            <td class="boxRight">' . "\n";
              $box = new box;
              echo $box->infoBox($heading, $contents);
              echo '            </td>' . "\n";
            }
          ?>
          </tr>
        </table>
      </td>
      <!-- body_text_eof //-->
    </tr>
  </table>
  <!-- body_eof //-->
  <!-- footer //-->
  <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
  <!-- footer_eof //-->
  <br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>